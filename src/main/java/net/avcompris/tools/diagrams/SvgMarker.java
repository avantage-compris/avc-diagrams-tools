package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;

class SvgMarker {

	public SvgMarker(final String id) {

		this.id = checkNotNull(id, "id");
	}

	public final String id;
}
