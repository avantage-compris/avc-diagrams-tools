package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.avcompris.domdumper.Dumper;
import net.avcompris.domdumper.XMLDumpers;

import com.avcompris.util.YamlUtils;
import com.avcompris.util.Yamled;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public abstract class DiagramTools {

	public static void translateYaml2SvgDiagram(final File yamlInFile,
			final File svgOutFile) throws IOException {

		checkNotNull(yamlInFile, "yamlInFile");
		checkNotNull(svgOutFile, "svgOutFile");

		final Yamled yamled = YamlUtils.loadYAML(yamlInFile);

		final String label = yamled.label();

		if ("svg".equals(label)) {

			yaml2svg(yamled, svgOutFile);

		} else if ("avc-diagram".equals(label)) {

			avcDiagram2svg(yamled, svgOutFile);

		} else {

			throw new IOException(
					"Label of YAML stream should be \"svg\" or \"avc-diagram\", but was: "
							+ label);
		}
	}

	public static void yaml2svg(final Yamled yamled, final File svgOutFile)
			throws IOException {

		final Yamled svg = yamled.get("svg");

		final String xmlns = svg.has("xmlns") //
		? svg.get("xmlns").asString()
				: "http://www.w3.org/2000/svg";

		final String version = svg.has("version") //
		? svg.get("version").asString()
				: "1.1";

		final OutputStream os = new FileOutputStream(svgOutFile);
		try {

			final boolean INDENT = true;

			final Dumper dumper = XMLDumpers.newDumper("svg",
					new String[] { "xmlns=" + xmlns }, new Object[] {
							"version", version }, INDENT, os, UTF_8);

			final Yaml2SvgTranslator translator = new Yaml2SvgTranslator(svg,
					dumper);

			translator.dumpAllNonXmlnsAttributesBut("version",
					"avc-diagrams-version");

			translator.dumpItems(yamled.items("content"));

			dumper.close();

		} finally {
			os.close();
		}
	}

	public static void avcDiagram2svg(final Yamled yamled,
			final File svgOutFile) throws IOException {

		final Yamled avcDiagram = yamled.get("avc-diagram");

		final OutputStream os = new FileOutputStream(svgOutFile);
		try {

			final boolean INDENT = true;

			final Dumper dumper = XMLDumpers.newDumper("svg",
					new String[] { "xmlns=http://www.w3.org/2000/svg" },
					new Object[] { "version", "1.1" }, INDENT, os, UTF_8);

			final AvcDiagram2SvgGenerator generator = new AvcDiagram2SvgGenerator(
					avcDiagram, dumper);

			generator.generate();

			dumper.close();

		} finally {
			os.close();
		}
	}

	public static void translateYaml2PdfDiagram(final File yamlInFile,
			final File pdfOutFile) throws IOException, DocumentException {

		checkNotNull(yamlInFile, "yamlInFile");
		checkNotNull(pdfOutFile, "pdfOutFile");

		final Yamled yamled = YamlUtils.loadYAML(yamlInFile);

		final String label = yamled.label();

		if ("avc-diagram".equals(label)) {

			avcDiagram2pdf(yamled, pdfOutFile);

		} else {

			throw new IOException(
					"Label of YAML stream should be \"avc-diagram\", but was: "
							+ label);
		}

	}

	public static void avcDiagram2pdf(final Yamled yamled,
			final File pdfOutFile) throws IOException, DocumentException {

		final Yamled avcDiagram = yamled.get("avc-diagram");

		final OutputStream os = new FileOutputStream(pdfOutFile);
		try {

			final Rectangle pageSize = PageSize.A4;
			final Document document = new Document(pageSize);
			final PdfWriter pdfWriter = PdfWriter.getInstance(document, os);

			document.open();

			final AvcDiagram2PdfGenerator generator = new AvcDiagram2PdfGenerator(
					avcDiagram, document, pdfWriter);

			generator.generate();

			document.close();

		} finally {
			os.close();
		}
	}
}
