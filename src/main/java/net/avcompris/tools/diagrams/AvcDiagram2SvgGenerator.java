package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.Yamled;

import net.avcompris.domdumper.Dumper;
import net.avcompris.tools.diagrams.desc.ArrowType;
import net.avcompris.tools.diagrams.desc.AvcLine;
import net.avcompris.tools.diagrams.desc.AvcRect;

class AvcDiagram2SvgGenerator extends AbstractAvcDiagram2XxxGenerator<Dumper> {

	private final Dumper rootDumper;

	public AvcDiagram2SvgGenerator(final Yamled rootYamled, final Dumper rootDumper) {

		super(rootYamled);

		this.rootDumper = checkNotNull(rootDumper, "rootDumper");
	}

	public void generate() throws IOException {

		rootDumper.addAttribute("width", rootYamled.get("width").asString());
		rootDumper.addAttribute("height", rootYamled.get("height").asString());

		loadSvgMarker(rootDumper, "arrow");

		parse(rootDumper, rootYamled.items("content"));
	}

	@Override
	protected void handle(final Dumper dumper, final AvcRect rect) throws IOException {

		final Dumper group = dumper.addElement("g", "class", "rect", "id", "rect_" + rect.id);

		group.addElement("rect") //
				.addAttribute("x", rect.x) //
				.addAttribute("y", rect.y) //
				.addAttribute("width", rect.width) //
				.addAttribute("height", rect.height) //
				.addAttribute("fill", rect.fill) //
				.addAttribute("stroke", rect.stroke) //
				.addAttribute("stroke-width", rect.strokeWidth);

		final Dumper t = group.addElement("text");
		t.addAttribute("x", rect.x + rect.width / 2);
		t.addAttribute("y", rect.y + rect.height / 2 + Double.parseDouble(rect.fontSize.replace("px", "")) * 0.3);
		t.addAttribute("fill", rect.textColor);
		t.addAttribute("text-anchor", "middle");
		t.addAttribute("font-size", rect.fontSize);
		t.addAttribute("font-family", rect.fontFamily);
		t.addCharacters(rect.text);
	}

	@Nullable
	private SvgMarker getArrowMarker(@Nullable final ArrowType arrowType) {

		if (arrowType == null) {
			return null;
		}

		switch (arrowType) {
		case ARROW:
			return getSvgMarker("arrow");
		default:
			throw new IllegalArgumentException("arrowType: " + arrowType);
		}
	}

	@Override
	protected void handle(final Dumper dumper, final AvcLine line) throws IOException {

		@Nullable
		final SvgMarker startMarker = getArrowMarker(line.startArrowType);
		@Nullable
		final SvgMarker endMarker = getArrowMarker(line.endArrowType);

		final Dumper l = dumper.addElement("line") //
				.addAttribute("x1", line.x1) //
				.addAttribute("y1", line.y1) //
				.addAttribute("x2", line.x2) //
				.addAttribute("y2", line.y2) //
				.addAttribute("stroke", line.stroke) //
				.addAttribute("stroke-width", line.strokeWidth);

		if (startMarker != null) {
			l.addAttribute("marker-start", "url(#" + startMarker.id + ")");
		}
		if (endMarker != null) {
			l.addAttribute("marker-end", "url(#" + endMarker.id + ")");
		}
	}

	private SvgMarker getSvgMarker(final String type) {

		final SvgMarker marker = svgMarkers.get(type);

		if (marker == null) {
			throw new IllegalStateException("Cannot find marker for type: " + type);
		}

		return marker;
	}

	private final Map<String, SvgMarker> svgMarkers = new HashMap<String, SvgMarker>();

	private SvgMarker loadSvgMarker(final Dumper dumper, final String type) throws IOException {

		if (svgMarkers.containsKey(type)) {
			throw new IllegalStateException("Marker has already been loaded for type: " + type);
		}

		final SvgMarker marker = new SvgMarker(type);

		svgMarkers.put(type, marker);

		if ("arrow".equals(type)) {

			dumper.addElement("marker").addAttribute("id", marker.id).addAttribute("markerWidth", 6)
					.addAttribute("markerHeight", 6).addAttribute("viewBox", "-3 -3 6 6").addAttribute("refX", 2.5)
					.addAttribute("refY", 0).addAttribute("markerUnits", "strokeWidth").addAttribute("orient", "auto") //
					.addElement("polygon").addAttribute("points", points( //
							1, 0, //
							-2, 2, //
							-2, 3, //
							2, 0.5, //
							2, -0.5, //
							-2, -3, //
							-2, -2 //
					)) //
					.addAttribute("fill", "#000000");

		} else {

			throw new NotImplementedException("marker.type: " + type);
		}

		return marker;
	}

	private static String points(final double... coordinates) {

		checkNotNull(coordinates, "coordinates");

		if (coordinates.length == 0) {
			throw new IllegalArgumentException("coordinates should not be an empty array");
		}

		if (coordinates.length % 2 != 0) {
			throw new IllegalArgumentException(
					"coordinates should be of even count: x0, y0, x1, y2, etc., but length was: " + coordinates.length);
		}

		final StringBuilder sb = new StringBuilder();

		for (int i = 0; i < coordinates.length; i += 2) {

			if (i != 0) {
				sb.append(' ');
			}

			sb.append(coordinates[i] + "," + coordinates[i + 1]);
		}

		return sb.toString();
	}
}
