package net.avcompris.tools.diagrams.desc;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

public class AvcRect implements AvcDesc {

	public final double x;
	public final double y;
	public final double width;
	public final double height;

	public final String id;
	@Nullable
	public final String fill;
	@Nullable
	public final String stroke;
	@Nullable
	public final Double strokeWidth;
	@Nullable
	public final String fontFamily;
	@Nullable
	public final String fontSize;
	@Nullable
	public final String textColor;
	@Nullable
	public final String text;

	public AvcRect(final String id, final double x, final double y, final double width, final double height,
			@Nullable final String fill, @Nullable final String stroke, @Nullable final Double strokeWidth,
			@Nullable final String fontFamily, @Nullable final String fontSize, @Nullable final String textColor,
			@Nullable final String text) {

		this.id = checkNotNull(id, "id");
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.fill = fill;
		this.stroke = stroke;
		this.strokeWidth = strokeWidth;
		this.fontFamily = fontFamily;
		this.fontSize = fontSize;
		this.textColor = textColor;
		this.text = text;
	}
}
