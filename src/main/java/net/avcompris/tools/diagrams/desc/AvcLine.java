package net.avcompris.tools.diagrams.desc;

import javax.annotation.Nullable;

public class AvcLine implements AvcDesc {

	public final double x1;
	public final double y1;
	public final double x2;
	public final double y2;
	
	@Nullable
	public final String stroke;
	@Nullable
	public final Double strokeWidth;

	@Nullable
	public final ArrowType startArrowType;
	@Nullable
	public final ArrowType endArrowType;

	public AvcLine(final double x1, final double y1, final double x2,
			final double y2, @Nullable final String stroke,
			@Nullable final Double strokeWidth,
			@Nullable final ArrowType startArrowType,
			@Nullable final ArrowType endArrowType			
			) {

		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.stroke = stroke;
		this.strokeWidth = strokeWidth;
		this.startArrowType = startArrowType;
		this.endArrowType = endArrowType;
	}
}
