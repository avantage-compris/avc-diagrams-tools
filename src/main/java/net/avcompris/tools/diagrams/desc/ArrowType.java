package net.avcompris.tools.diagrams.desc;

public enum ArrowType {

	ARROW(1);

	private ArrowType(final int id) {

		this.id = id;
	}

	private final int id;

	public static ArrowType byId(int id) {

		for (final ArrowType arrowType : values()) {

			if (arrowType.id == id) {
				return arrowType;
			}
		}

		throw new IllegalArgumentException("Unkown id: " + id);
	}
}
