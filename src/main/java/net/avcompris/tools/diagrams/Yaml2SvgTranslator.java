package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Map;

import net.avcompris.domdumper.Dumper;

import com.avcompris.util.Yamled;

class Yaml2SvgTranslator {

	private final Yamled rootYamled;
	private final Dumper rootDumper;

	public Yaml2SvgTranslator(final Yamled rootYamled, final Dumper rootDumper) {

		this.rootYamled = checkNotNull(rootYamled, "rootYamled");
		this.rootDumper = checkNotNull(rootDumper, "rootDumper");
	}

	public void dumpAllNonXmlnsAttributesBut(
			final String... excludeAttributeNames) throws IOException {

		dumpAllNonXmlnsAttributesBut(rootDumper,rootYamled, 
				excludeAttributeNames);
	}

	private static void dumpAllNonXmlnsAttributesBut(
			final Dumper dumper, final Yamled yamled,final String... excludeAttributeNames)
			throws IOException {

		loop: for (final Map.Entry<?, ?> entry : ((Map<?, ?>) yamled)
				.entrySet()) {

			final String key = entry.getKey().toString();

			if ("content".equals(key) || key.startsWith("xmlns")) {
				continue;
			}

			for (final String excludeAttributeName : excludeAttributeNames) {

				if (excludeAttributeName.equals(key)) {
					continue loop;
				}
			}

			dumper.addAttribute(key, yamled.get(key).asString());
		}
	}

	public void dumpItems(final Yamled... items) throws IOException {

		dumpItems(rootYamled, rootDumper, items);
	}

	private void dumpItems(final Yamled yamled, final Dumper dumper,
			final Yamled... items) throws IOException {

		for (final Yamled item : items) {

			final String label = item.label();

			final Yamled itemYamled = item.get(label);

			if ("style".equals(label) || "text".equals(label)) {

				dumpWithTextContent(dumper, label, itemYamled);

			} else if ("line".equals(label)) {

				dumpWithNoContent(dumper, label, itemYamled);

			} else {

				throw new IOException("Unknown label: " + label);
			}
		}
	}

	private static void dumpWithTextContent(final Dumper dumper, final String label,
			final Yamled yamled) throws IOException {

		final Dumper sub = dumper.addElement(label);

		dumpAllNonXmlnsAttributesBut(sub,yamled);

		sub.addCharacters(yamled.get("content").asString());
	}

	private static void dumpWithNoContent(final Dumper dumper,
			final String label, final Yamled yamled) throws IOException {

		final Dumper sub = dumper.addElement(label);

		dumpAllNonXmlnsAttributesBut(sub,yamled);
	}
}
