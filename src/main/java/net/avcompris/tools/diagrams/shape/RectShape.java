package net.avcompris.tools.diagrams.shape;

public class RectShape implements Shape {

	public RectShape(final String id, final double x, final double y,
			final double width, final double height) {

		this.id = id;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	private final String id;
	private final double x;
	private final double y;
	private final double width;
	private final double height;

	@Override
	public String getId() {

		return id;
	}

	@Override
	public double getEdgeX(final double px, final double py) {

		return x + px * width;
	}

	@Override
	public double getEdgeY(final double px, final double py) {

		return y + py * height;
	}
}
