package net.avcompris.tools.diagrams.shape;

public interface Shape {

	String getId();
	
	double getEdgeX(double px, double py);
	
	double getEdgeY(double px, double py);
}
