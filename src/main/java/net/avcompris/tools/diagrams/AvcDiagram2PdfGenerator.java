package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.tools.diagrams.desc.ArrowType.ARROW;

import java.io.IOException;

import net.avcompris.tools.diagrams.desc.AvcLine;
import net.avcompris.tools.diagrams.desc.AvcRect;

import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.Yamled;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

class AvcDiagram2PdfGenerator extends AbstractAvcDiagram2XxxGenerator<Document> {

	public AvcDiagram2PdfGenerator(final Yamled rootYamled,
			final Document document, final PdfWriter pdfWriter) {

		super(rootYamled);

		this.document = checkNotNull(document, "document");

		// final PdfWriter pdfWriter =
		checkNotNull(pdfWriter, "pdfWriter");

		canvas = pdfWriter.getDirectContent();
	}

	private final Document document;
	// private final PdfWriter pdfWriter;
	private final PdfContentByte canvas;

	public void generate() throws IOException {

		parse(document, rootYamled.items("content"));

		canvas.fill();
	}

	private static float coordX(final double x) {

		return (float) x;
	}

	private static float coordY(final double y) {

		return 800 - (float) y;
	}

	private static float coordWidth(final double width) {

		return (float) width;
	}

	@Override
	protected void handle(final Document callback, final AvcRect rect)
			throws IOException {

		final Rectangle pdfRect = new Rectangle(coordX(rect.x), coordY(rect.y),
				coordX(rect.x + rect.width), coordY(rect.y + rect.height));

		pdfRect.setBorder(Rectangle.BOX);
		pdfRect.setBorderWidth(coordWidth(rect.strokeWidth));

		canvas.rectangle(pdfRect);

		final ColumnText columnText = new ColumnText(canvas);

		final Rectangle pdfRect2 = new Rectangle(coordX(rect.x), coordY(rect.y
				+ rect.height / 2 - 15), coordX(rect.x + rect.width),
				coordY(rect.y + rect.height / 2 + 10));

		columnText.setSimpleColumn(pdfRect2);

		final Paragraph p = new Paragraph(rect.text);

		p.setAlignment(Element.ALIGN_CENTER);

		columnText.addElement(p);

		try {

			columnText.go();

		} catch (final DocumentException e) {

			throw new RuntimeException(e);
		}
	}

	@Override
	protected void handle(final Document callback, final AvcLine line)
			throws IOException {

		if (colorEquals(line.stroke, "#000000")) {

			canvas.setColorStroke(BaseColor.BLACK);

		} else {

			throw new NotImplementedException("line.stroke: " + line.stroke);
		}

		final double dx = line.x2 - line.x1;
		final double dy = line.y2 - line.y1;
		final double dist = Math.sqrt(dx * dx + dy * dy);

		if (dist == 0.0) {
			return;
		}

		final double cos = dx / dist;
		final double sin = dy / dist;

		canvas.setLineWidth(line.strokeWidth);

		canvas.moveTo(coordX(line.x1), coordY(line.y1));

		final double k = line.strokeWidth;

		if (line.endArrowType == null) {

			canvas.lineTo(coordX(line.x2), coordY(line.y2));

		} else if (line.endArrowType == ARROW) {

			canvas.lineTo(coordX(line.x2 - k * cos), coordY(line.y2 + k * sin));

		} else {

			throw new IllegalStateException("Unkown endArrowType: "
					+ line.endArrowType);
		}

		canvas.closePathStroke();

		if (line.endArrowType == ARROW) {

			final double kx = k * 5;
			final double ky = k * 2.5;

			canvas.moveTo(coordX(line.x2 - kx * cos - ky * sin), coordY(line.y2
					- kx * sin + ky * cos));
			canvas.lineTo(coordX(line.x2 - k * 1 * cos), coordY(line.y2 - k * 1
					* sin));
			canvas.lineTo(coordX(line.x2 - kx * cos + ky * sin), coordY(line.y2
					- kx * sin - ky * cos));

			canvas.stroke();
		}
	}
}
