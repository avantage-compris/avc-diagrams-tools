package net.avcompris.tools.diagrams;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.Yamled;

import net.avcompris.tools.diagrams.desc.ArrowType;
import net.avcompris.tools.diagrams.desc.AvcLine;
import net.avcompris.tools.diagrams.desc.AvcRect;
import net.avcompris.tools.diagrams.shape.RectShape;
import net.avcompris.tools.diagrams.shape.Shape;

abstract class AbstractAvcDiagram2XxxGenerator<T> {

	protected final Yamled rootYamled;

	protected AbstractAvcDiagram2XxxGenerator(final Yamled rootYamled) {

		this.rootYamled = checkNotNull(rootYamled, "rootYamled");
	}

	private final Map<String, Shape> shapes = new HashMap<String, Shape>();

	protected static double getDouble(final Yamled yamled, final String propertyName) {

		final Yamled propertyValue = yamled.get(propertyName);

		if (propertyValue == null) {

			throw new IllegalStateException(propertyName + ".value is null");

		} else if (propertyValue.isDouble()) {

			return propertyValue.asDouble();

		} else if (propertyValue.isLong()) {

			return propertyValue.asLong();

		} else {

			throw new IllegalStateException(
					propertyName + ".value.type: " + propertyValue.getClass().getName() + " (" + propertyValue + ")");
		}
	}

	protected static String getColor(final Yamled yamled, final String propertyName) {

		final Yamled propertyValue = yamled.get(propertyName);

		if (propertyValue == null) {

			throw new IllegalStateException(propertyName + ".value is null");
		}

		final String v = propertyValue.asString();

		if (v.startsWith("#")) {

			return v;

		} else if (v.startsWith("\\#")) {

			return v.substring(1);

		} else {

			return v;
		}
	}

	protected abstract void handle(T callback, AvcRect rect) throws IOException;

	protected abstract void handle(T callback, AvcLine line) throws IOException;

	protected final void parse(final T callback, final Yamled... items) throws IOException {

		// 1. POPULATE THE SHAPES

		for (final Yamled item : items) {

			final String label = item.label();

			if (label.startsWith("rect_")) {

				final double x = getDouble(item, "x");
				final double y = getDouble(item, "y");
				final double width = getDouble(item, "width");
				final double height = getDouble(item, "height");

				if (shapes.containsKey(label)) {
					throw new IOException("Duplicate id: " + label);
				}

				shapes.put(label, new RectShape(label, x, y, width, height));
			}
		}

		// 2. USE THE SHAPES

		for (final Yamled item : items) {

			final String label = item.label();

			if (label.startsWith("rect_")) {

				final double x = getDouble(item, "x");
				final double y = getDouble(item, "y");
				final double width = getDouble(item, "width");
				final double height = getDouble(item, "height");

				final Yamled shape = item.get("shape");

				final String fill = getColor(shape, "fill");
				final String stroke = getColor(shape, "stroke");
				final double strokeWidth = getDouble(shape, "stroke-width");

				final Yamled text = item.get("text");

				final String textColor = getColor(text, "color");
				final String fontSize = text.get("font-size").asString();
				final String fontFamily = text.get("font-family").asString();
				final String content = text.get("content").asString();

				final String id = label;

				final AvcRect rect = new AvcRect(id, x, y, width, height, fill, stroke, strokeWidth, fontFamily,
						fontSize, textColor, content);

				handle(callback, rect);

			} else if ("line".equals(label)) {

				// final Yamled line = item.get("line");

				final String stroke = getColor(item, "stroke");
				final double strokeWidth = getDouble(item, "stroke-width");

				final double x1;
				final double y1;
				final double x2;
				final double y2;

				final Yamled from = item.get("from");
				final Yamled to = item.get("to");

				final String fromId = from.get("id").asString();
				final String toId = to.get("id").asString();

				final Shape fromShape = shapes.get(fromId);
				final Shape toShape = shapes.get(toId);

				final double fromPX = getDouble(from, "px");
				final double fromPY = getDouble(from, "py");
				final double toPX = getDouble(to, "px");
				final double toPY = getDouble(to, "py");

				x1 = fromShape.getEdgeX(fromPX, fromPY);
				y1 = fromShape.getEdgeY(fromPX, fromPY);
				x2 = toShape.getEdgeX(toPX, toPY);
				y2 = toShape.getEdgeY(toPX, toPY);

				final ArrowType startArrowType = from.has("arrowType") //
						? ArrowType.byId(from.get("arrowType").asInt())
						: null;
				final ArrowType endArrowType = to.has("arrowType") //
						? ArrowType.byId(to.get("arrowType").asInt())
						: null;

				final AvcLine line = new AvcLine(x1, y1, x2, y2, stroke, strokeWidth, startArrowType, endArrowType);

				handle(callback, line);

			} else {

				throw new NotImplementedException();
			}
		}
	}

	public static boolean colorEquals(final String color1, final String color2) {

		checkNotNull(color1, "color1");
		checkNotNull(color2, "color2");

		if (color1.equals(color2)) {
			return true;
		}

		throw new NotImplementedException("color1: " + color1 + ", color2: " + color2);
	}
}
