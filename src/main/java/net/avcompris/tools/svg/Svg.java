package net.avcompris.tools.svg;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;

@XPath("/svg:svg")
@Namespaces({ "xmlns:svg=http://www.w3.org/2000/svg",
		"xmlns:xlink=http://www.w3.org/1999/xlink" })
public interface Svg {

	@XPath("@version")
	String getVersion();
}
