package net.avcompris.tools.diagrams;

import static org.junit.Assert.assertNotEquals;

import java.io.File;

import org.junit.After;
import org.junit.Test;

public class Yaml2PDFDiagramTest {

	private File yamlFile;
	private File pdfFile;

	@After
	public void tearDown() throws Exception {

		assertNotEquals(0, pdfFile.length());
	}

	@Test
	public void test003_avcDiagYaml() throws Exception {

		yamlFile = new File("src/test/yaml", "003-avc.diag.yaml");

		pdfFile = new File("target", yamlFile.getName() + ".pdf");

		DiagramTools.translateYaml2PdfDiagram(yamlFile, pdfFile);
	}
}