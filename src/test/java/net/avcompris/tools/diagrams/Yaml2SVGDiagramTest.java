package net.avcompris.tools.diagrams;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;

import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.tools.diagrams.DiagramTools;
import net.avcompris.tools.svg.Svg;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;

public class Yaml2SVGDiagramTest {

	private File yamlFile;
	private File svgFile;

	@After
	public void tearDown() throws Exception {

		System.out.println(FileUtils.readFileToString(svgFile, UTF_8));

		final Svg svg = DomBinderUtils.xmlContentToJava(svgFile, Svg.class);

		assertEquals("1.1", svg.getVersion());
	}

	@Test
	public void test001_fqcSvgYaml() throws Exception {

		yamlFile = new File("src/test/yaml", "001-fqc.svg.yaml");

		svgFile = new File("target", yamlFile.getName() + ".svg");

		DiagramTools.translateYaml2SvgDiagram(yamlFile, svgFile);
	}

	@Test
	public void test002_fqcSvg_defaultDeclsYaml() throws Exception {

		yamlFile = new File("src/test/yaml", "002-fqc.svg_defaultDecls.yaml");

		svgFile = new File("target", yamlFile.getName() + ".svg");

		DiagramTools.translateYaml2SvgDiagram(yamlFile, svgFile);
	}

	@Test
	public void test003_avcDiagYaml() throws Exception {

		yamlFile = new File("src/test/yaml", "003-avc.diag.yaml");

		svgFile = new File("target", yamlFile.getName() + ".svg");

		DiagramTools.translateYaml2SvgDiagram(yamlFile, svgFile);
	}
}